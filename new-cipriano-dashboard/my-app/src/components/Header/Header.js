import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

const Header = () => (
  <div style={{ flexGrow: 1 }}>
    <AppBar position="static" style={{ backgroundColor: "#511b66" }}>
      <Toolbar>
        <Typography variant="title" color="inherit" style={{ flex: 1 }}>
          Resto Loyal dashboard
        </Typography>
        <Typography variant="title" color="inherit">
          Cipriano Wine & Ham
        </Typography>
      </Toolbar>
    </AppBar>
  </div>
);
export default Header;
