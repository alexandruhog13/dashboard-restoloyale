const styles = theme => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.primary
  },
  secondaryHeading: {
    fontSize: "18px",
    alignItems: "center",
    color: "#d5ede8"
  },
  icon: {
    verticalAlign: "center",
    height: 20,
    width: 20
  },
  details: {
    alignItems: "center"
  },
  panelColumn: {
    flexBasis: "33.33%"
  },
  grid: {
    marginTop: 10,
    marginRight: "30%",
    marginLeft: "30%"
  }
});

export default styles;
