import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import styles from "./styles.js";
import Orders from "../Orders/Orders.js";
import CallWaiter from "../Call Waiter/CallWaiter.js";
import Typography from "@material-ui/core/Typography";

import { subscribeToEvent } from "../../api/io.js";

class Events extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      calls: []
    };
    subscribeToEvent((err, details) => {
      if (details.order === undefined) {
        let events = this.state.calls;
        events.push(details);
        this.setState({
          calls: events
        });
      } else {
        let events = this.state.orders;
        events.push(details);
        this.setState({
          orders: events
        });
      }
    });
  }
  handleCloseClick = (e, index, type) => {
    if (type === "call") {
      let events = this.state.calls;
      events.splice(index, 1);
      this.setState({
        calls: events
      });
    } else {
      let events = this.state.orders;
      events.splice(index, 1);
      this.setState({
        orders: events
      });
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid xs={6} item container>
            <CallWaiter
              events={this.state.calls}
              handleCloseClick={this.handleCloseClick}
            />
          </Grid>
          <Grid xs={6} item container>
            <Orders
              events={this.state.orders}
              handleCloseClick={this.handleCloseClick}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Events);
