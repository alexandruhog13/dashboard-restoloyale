import React, { Component } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Events from "../Events/Events.js";
import Header from "../Header/Header.js";

import "./App.css";

class App extends Component {
  render() {
    return (
      <React.Fragment >
        <CssBaseline />
        <Header />
        <Events/>
      </React.Fragment>
    );
  }
}

export default App;
