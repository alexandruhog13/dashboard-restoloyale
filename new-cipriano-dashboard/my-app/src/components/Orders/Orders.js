import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import ExpansionPanelActions from "@material-ui/core/ExpansionPanelActions";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import styles from "./styles.js";

class Orders extends Component {
  render() {
    const { classes, events, handleCloseClick } = this.props;
    return (
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="stretch"
      >
        <h3 align="center">Orders</h3>
        {events.map((el, index) => {
          return (
            <Grid key={index} className={classes.grid} item>
              <ExpansionPanel
                style={{
                  backgroundColor: "#0940a5"
                }}
              >
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <div className={classes.panelColumn}>
                    <Typography className={classes.secondaryHeading}>
                      {`Table ${el.tableId}`}
                    </Typography>
                  </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails className={classes.details}>
                  <List>
                    {el.order.map(product => (
                      <ListItem key={product.name} divider>
                        <ListItemText
                          primary={
                            <Typography className={classes.Product}>
                              {product.name}
                            </Typography>
                          }
                          secondary={
                            <Typography className={classes.Count}>
                              {`${product.count} portions`}
                            </Typography>
                          }
                        />
                      </ListItem>
                    ))}
                  </List>
                </ExpansionPanelDetails>
                <Divider />
                <ExpansionPanelActions>
                  <IconButton
                    aria-label="Delete"
                    className={classes.icon}
                    onClick={e => handleCloseClick(e, index, "order")}
                  >
                    <DeleteIcon />
                  </IconButton>
                </ExpansionPanelActions>
              </ExpansionPanel>
            </Grid>
          );
        })}
      </Grid>
    );
  }
}

export default withStyles(styles)(Orders);
