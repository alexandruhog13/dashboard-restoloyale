const styles = theme => ({
  root: {
    width: "100%"
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    color: "#d5ede8"
  },
  secondaryHeading: {
    fontSize: "18px",
    alignItems: "center",
    color: "#d5ede8"
  },
  icon: {
    verticalAlign: "center",
    height: 20,
    width: 20
  },
  details: {
    alignItems: "center"
  },
  panelColumn: {
    // flexBasis: "33.33%"
  },
  grid: {
    marginTop: 10,
    marginRight: "10%",
    marginLeft: "10%"
  },
  Product:{
    color: "#d5ede8",
    fontSize: "18px",
    alignItem: 'left'
  },
  Count:{
    color: "#d5ede8",
    fontSize: "20px",
    // align: 'right'
  }
});

export default styles;
