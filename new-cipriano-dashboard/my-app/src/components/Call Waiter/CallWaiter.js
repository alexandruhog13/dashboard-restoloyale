import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import styles from "./styles.js";
import Typography from "@material-ui/core/Typography";

class CallWaiter extends Component {
  render() {
    const { classes, events, handleCloseClick } = this.props;
    return (
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="stretch"
      >
        <h3 align="center">Call Waiter</h3>
        {events.map((el, index) => (
          <Grid key={index} className={classes.grid} item>
            <List style={{ backgroundColor: "#0b913a" }}>
              <ListItem>
                <ListItemText
                  primary={
                    <Typography className={classes.secondaryHeading}>
                      {`Table ${el.tableId}
                      Type ${el.type}`}
                    </Typography>
                  }
                />
                <ListItemSecondaryAction>
                  <IconButton
                    aria-label="Delete"
                    className={classes.icon}
                    onClick={e => handleCloseClick(e, index, "call")}
                  >
                    <DeleteIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            </List>
          </Grid>
        ))}
      </Grid>
    );
  }
}

export default withStyles(styles)(CallWaiter);
