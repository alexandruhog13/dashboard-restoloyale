import openSocket from "socket.io-client";

const socket = openSocket("http://18.217.131.26");

export const subscribeToEvent = cb => {
  console.log('Got something');
  socket.on("place-event", details => cb(null, details));
};
